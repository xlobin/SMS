Ext.define('SMS.view.Login', {// #1
    extend: 'Ext.window.Window', // #2
    alias: 'widget.login',
    // #3
    autoShow: true,
    height: 170,
    width: 360,
    layout: {
        type: 'fit'
    },
    iconCls: 'key',
    title: "Login",
    closeAction: 'hide',
    closable: false,
    items: [
        {
            xtype: 'form',
// #12
            frame: false,
// #13
            bodyPadding: 15,
// #14
            defaults: {
// #15
                xtype: 'textfield', // #16
                anchor: '100%',
// #17
                labelWidth: 60
// #18
            },
            items: [
                {
                    name: 'user',
                    fieldLabel: "User",
                    allowBlank: false,
                    vtype: 'alphanum',
                    minLength: 3,
                    msgTarget: 'under',
                    maxLength: 25
// #20
// #21
// #22
// #23

                },
                {
                    inputType: 'password', // #19
                    name: 'password',
                    fieldLabel: "Password",
                    allowBlank: false,
                    vtype: 'alphanum',
                    minLength: 3,
                    msgTarget: 'under',
                    maxLength: 15,
                    enableKeyEvents: true,
                    id: 'password'


// #20
// #21
// #22
// #23
                }
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'bottom',
                    items: [
                        {
                            xtype: 'tbfill' //#24
                        },
                        {
                            xtype: 'button', // #25
                            itemId: 'cancel',
                            iconCls: 'cancel',
                            text: 'Cancel'
                        },
                        {
                            xtype: 'button', // #26
                            itemId: 'submit',
                            formBind: true, // #27
                            iconCls: 'key-go',
                            text: "Submit"
                        }
                    ]
                }
            ]
        }
    ],
// #4
// #5
// #6
// #7
// #8
//#9
//#10
//#11




});

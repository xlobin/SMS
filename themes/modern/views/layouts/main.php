<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />

        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/resources/css/app.css">
        <!-- <x-compile> -->
            <!-- <x-bootstrap> -->
            <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/bootstrap.css">
            <script src="<?php echo Yii::app()->request->baseUrl; ?>/ext/ext-dev.js"></script>
            <script src="<?php echo Yii::app()->request->baseUrl; ?>/bootstrap.js"></script>
            <!-- </x-bootstrap> -->
            <script src="<?php echo Yii::app()->request->baseUrl; ?>/app.js"></script>
        <!-- </x-compile> -->
    </head>
    <body>
    </body>
</html>

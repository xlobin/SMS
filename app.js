/*
 This file is generated and updated by Sencha Cmd. You can edit this file as
 needed for your application, but these edits will have to be merged by
 Sencha Cmd when upgrading.
 */
Ext.application({
    name: 'SMS',
    enableQuickTips: true,
    init: function() {
        splashscreen = Ext.getBody().mask('Loading application',
                'splashscreen');
        /*
         splashscreen.addCls('splashscreen');
         Ext.DomHelper.insertFirst(Ext.query('.x-mask-msg')[0], {
         cls: 'x-splash-icon'
         });
         */
        /*
         Ext.apply(Ext.form.field.VTypes, {
         customPass: function(val, field) {
         return /^((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})/.
         test(val);
         },
         customPassText: 'Not a valid password. Length must be at least' +
         '6 characters and maximum of 20Password must contain one digit, one' +
         'letter lowercase, one letter uppercase, onse special symbol @#$ % and' +
         'between 6 and 20 characters.',
         });
         */
    },
    launch: function() { // #3
        Ext.tip.QuickTipManager.init();
        var task = new Ext.util.DelayedTask(function() {
            splashscreen.fadeOut({
                duration: 1000,
                remove: true
            });
            splashscreen.next().fadeOut({
                duration: 1000,
                remove: true,
                listeners: {
                    afteranimate: function(el, startTime, eOpts) {
                        Ext.widget('login'); // #1
                    }
                }
            });
            //console.log('launch'); // #1
        });
        task.delay(2000);
    },
    controllers: [
        'Login'
    ]
    /*extend: 'SMS.Application',  
     autoCreateViewport: true
     */
});